from test_framework._dv_interface import dv_interface
from test_framework._support import *
import dv.Control as Control
from test_framework._test_params import *


# Convenience Test: check if a camera is connected to the test machine.
@standard_params
def test_00_02_camera_check(self):
    # Starting runtime
    mytest = dv_interface(test_params["name"] + "_log.txt")

    # Remove all modules if exists
    mytest.remove_all_modules()

    # Add dv_davis to get the discover-davis-device hook.
    mytest.add_module(dv_module("davis", library="dv_davis"))

    # Run Update device list
    mytest.put("/system/devices/", "updateAvailableDevices", "true")

    if len(mytest.get_children('/system/devices/')) > 0:
        os.environ['DV_TEST_CAMERA_CONNECTED'] = "True"
    else:
        os.environ['DV_TEST_CAMERA_CONNECTED'] = "False"

    closing = mytest.close(runtime_output=None)
    assert_close(self, closing)
