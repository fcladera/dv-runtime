from test_framework._support import *
from test_framework._dv_interface import dv_interface
from test_framework._test_params import *


@standard_params
def test_03_controlPutGetLimitLogSize(self):
    # Test name
    test_name = test_params["name"]

    # Check Run
    check_run(self)

    # Start time
    start = time.time()

    mytest = dv_interface(test_params["name"] + "_log.txt")
    mytest.remove_all_modules()

    mytest.put("/system/logger/", "limitLogSize", 2000)
    self.assertEqual(mytest.get("/system/logger/", "limitLogSize"), 2000)
    mytest.put("/system/logger/", "limitLogSize", 5000)
    self.assertEqual(mytest.get("/system/logger/", "limitLogSize"), 5000)

    runtime_output = []
    closing = mytest.close(runtime_output=runtime_output)
    assert_close(self, closing)

    # Sending data to database
    upload_results_to_SQL_DB(total_test_time=round((time.time() - start) * 1000), runtime=runtime_output[0])
