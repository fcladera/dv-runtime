from test_framework._support import *
from test_framework._test_params import *


@davis_input
# Looks like cv::resize(INTER_LINEAR) has different optimizations on MacOS ARM.
# Therefore we need to provide different expected results for each platform.
@output_hash([
    "6ca05e72672bb8025977e3ebb8ec61205909499d43d013ed076fbd41deea60fb",  # All others
    "a8ce47979b10d8e85c9c26116323123fe98f7d1b6ec7451fd40b23336dbf6bd5"  # OS X ARM
])
def test_07_02_CropScaleFrames(self):
    module_parameters = [["cropOffsetX", "50"], ["cropWidth", "100"], ["cropOffsetY", "50"], ["cropHeight", "100"],
                         ["resizeWidth", "200"], ["resizeHeight", "200"], ["resize", "true"]]
    module = dv_module("dv_crop_scale", [["frames", "input[frames]"]], ["frames"], config_options=module_parameters)
    test_modules_with_io(self, [module])
