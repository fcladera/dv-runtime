from test_framework._support import *
from test_framework._test_params import *


@davis_input
@output_hash("22332b7c04401a80f197024af5f298a1de1cd83ee4cf13d09bb7215371517f8d")
def test_08_02_FlipRotateFramesHVD(self):
    module_parameters = [["degreeOfRotation", "90"], ["frameFlipVertically", "true"], ["frameFlipHorizontally", "true"]]
    module = dv_module("dv_flip_rotate", [["frames", "input[frames]"]], ["frames"], config_options=module_parameters)
    test_modules_with_io(self, [module])
