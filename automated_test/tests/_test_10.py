from test_framework._support import *
from test_framework._test_params import *


@davis_input
@output_hash("acae6ea9eb46b845a0d982a6803eaa559bb35598f1a5a70b9933f9ddba76138f")
def test_10_VideoOutputFFV1(self):
    output_file = create_output_file_name_mkv()
    module_parameters = [["fileName", output_file], ["codec", "FFV1_[ffv1]"]]
    module = dv_module("output0", [["frames", "input[frames]"]],
                       config_options=module_parameters,
                       library="dv_video_output")
    test_modules_with_input(self, [module])
