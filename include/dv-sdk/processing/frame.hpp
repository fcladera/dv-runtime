#ifndef DV_PROCESSING_FRAME_HPP
#define DV_PROCESSING_FRAME_HPP

#include "core.hpp"

namespace dv {

/**
 * Common accumulator class that allows to accumulate events into a frame.
 * The class is highly configurable to adapt to various use cases. This
 * is the preferred functionality for projecting events onto a frame.
 *
 * Accumulation of the events is performed on a floating point frame,
 * with every event contributing a fixed amount to the potential. Timestamps
 * of the last contributions are stored as well, to allow for a decay.
 */
class Accumulator {
public:
	/**
	 * Decay function to be used to decay the surface potential.
	 *
	 * * `NONE`: Do not decay at all. The potential can be reset manually
	 *    by calling the `clear` function
	 *
	 * * `LINEAR`: Perform a linear decay with  given slope. The linear decay goes
	 *    from currentpotential until the potential reaches the neutral potential
	 *
	 * * `EXPONENTIAL`: Exponential decay with time factor tau. The potential
	 *    eventually converges to zero.
	 *
	 * * `STEP`: Decay sharply to neutral potential after the given time.
	 *    Constant potential before.
	 */
	enum Decay { NONE = 0, LINEAR = 1, EXPONENTIAL = 2, STEP = 3 };

private:
	// input
	bool rectifyPolarity_    = false;
	float eventContribution_ = .0;
	float maxPotential_      = .0;
	float neutralPotential_  = .0;
	float minPotential_      = .0;

	// decay
	Decay decayFunction_   = NONE;
	double decayParam_     = .0;
	bool synchronousDecay_ = false;

	// output
	cv::Size shape_;

	// state
	TimeMat decayTimeSurface_;
	cv::Mat potentialSurface_;
	int64_t highestTime_ = 0;

	// internal use methods
	/**
	 * __INTERNAL_USE_ONLY__
	 * Decays the potential at coordinates x, y to the given time, respecting the
	 * decay function. Updates the time surface to the last decay.
	 * @param x The x coordinate of the value to be decayed
	 * @param y The y coordinate of the value to be decayed
	 * @param time The time to which the value should be decayed to.
	 */
	void decay(int16_t x, int16_t y, int64_t time) {
		// normal handling for all the other functions
		int64_t lastDecayTime = decayTimeSurface_.at(y, x);
		assert(lastDecayTime <= time);

		float lastPotential = potentialSurface_.at<float>(y, x);
		switch (decayFunction_) {
			case LINEAR: {
				potentialSurface_.at<float>(y, x)
					= (lastPotential >= neutralPotential_)
						  ? std::max(lastPotential
										 - static_cast<float>(static_cast<double>(time - lastDecayTime) * decayParam_),
							  neutralPotential_)
						  : std::min(lastPotential
										 + static_cast<float>(static_cast<double>(time - lastDecayTime) * decayParam_),
							  neutralPotential_);
				decayTimeSurface_.at(y, x) = time;
				break;
			}

			case EXPONENTIAL: {
				potentialSurface_.at<float>(y, x)
					= ((lastPotential - neutralPotential_)
						  * static_cast<float>(
							  expf(-(static_cast<float>(time - lastDecayTime)) / static_cast<float>(decayParam_))))
					  + neutralPotential_;
				decayTimeSurface_.at(y, x) = time;
				break;
			}

			case STEP:
				// step is handled at the frame generation time
			case NONE:
			default: {
				break;
			}
		}
	}

	/**
	 * __INTERNAL_USE_ONLY__
	 * Contributes the effect of a single event onto the potential surface.
	 * @param x The x coordinate of where to contribute to
	 * @param y The y coordinate of where to contribute to
	 * @param polarity The polarity of the contribution
	 * @param time The event time at which the contribution happened
	 */
	void contribute(int16_t x, int16_t y, bool polarity) {
		float lastPotential = potentialSurface_.at<float>(y, x);
		float contribution  = eventContribution_;
		if (!rectifyPolarity_ && !polarity) {
			contribution = -contribution;
		}

		float newPotential = std::min(std::max(lastPotential + contribution, minPotential_), maxPotential_);
		potentialSurface_.at<float>(y, x) = newPotential;
	}

public:
	/**
	 * Silly default constructor. This generates an accumulator with zero size.
	 * An accumulator with zero size does not work. This constructor just exists
	 * to make it possible to default initialize an Accumulator to later redefine.
	 */
	Accumulator() = default;

	/**
	 * Accumulator constructor
	 * Creates a new Accumulator with the given params. By selecting the params
	 * the right way, the Accumulator can be used for a multitude of applications.
	 * The class also provides static factory functions that adjust the parameters
	 * for common use cases.
	 *
	 * @param size The size of the resulting frame. This must be at least the
	 * dimensions of the eventstream supposed to be added to the accumulator,
	 * otherwise this will result in memory errors.
	 * @param decayFunction The decay function to be used in this accumulator.
	 * The decay function is one of `NONE`, `LINEAR`, `EXPONENTIAL`, `STEP`. The
	 * function behave like their mathematical definitions, with LINEAR AND STEP
	 * going back to the `neutralPotential` over time, EXPONENTIAL going back to 0.
	 * @param decayParam The parameter to tune the decay function. The parameter has
	 * a different meaning depending on the decay function chosen:
	 * `NONE`: The parameter is ignored
	 * `LINEAR`: The paramaeter describes the (negative) slope of the linear function
	 * `EXPONENTIAL`: The parameter describes tau, by which the time difference is divided.
	 * @param synchronousDecay if set to true, all pixel values get decayed to the same time
	 * as soon as the frame is generated. If set to false, pixel values remain at the state
	 * they had when the last contribution came in.
	 * @param eventContribution The contribution a single event has onto the potential
	 * surface. This value gets interpreted positively or negatively depending on the
	 * event polarity
	 * @param maxPotential The upper cut-off value at which the potential surface
	 * is clipped
	 * @param neutralPotential The potential the decay function converges to over time.
	 * @param minPotential The lower cut-off value at which the potential surface
	 * is clipped
	 * @param rectifyPolarity Describes if the polarity of the events should be kept
	 * or ignored. If set to true, all events behave like positive events.
	 */
	Accumulator(const cv::Size &size, Accumulator::Decay decayFunction, double decayParam, bool synchronousDecay,
		float eventContribution, float maxPotential, float neutralPotential, float minPotential, bool rectifyPolarity) :
		rectifyPolarity_(rectifyPolarity),
		eventContribution_(eventContribution),
		maxPotential_(maxPotential),
		neutralPotential_(neutralPotential),
		minPotential_(minPotential),
		decayFunction_(decayFunction),
		decayParam_(decayParam),
		synchronousDecay_(synchronousDecay),
		shape_(size),
		decayTimeSurface_(TimeMat(size)),
		potentialSurface_(cv::Mat(size, CV_32F, static_cast<double>(neutralPotential))),
		highestTime_(0) {
	}

	/**
	 * Accumulates all the events in the supplied packet and puts them onto the
	 * accumulation surface.
	 * @param packet The packet containing the events that should be
	 * accumulated.
	 */
	void accumulate(const EventStore &packet) {
		if (potentialSurface_.empty()) {
			return;
		}

		if (packet.isEmpty()) {
			return;
		}

		if (decayFunction_ == NONE || decayFunction_ == STEP) {
			// for step and none, only contribute
			for (const Event &event : packet) {
				contribute(event.x(), event.y(), event.polarity());
			}
		}
		else {
			// for all others, decay before contributing
			for (const Event &event : packet) {
				decay(event.x(), event.y(), event.timestamp());
				contribute(event.x(), event.y(), event.polarity());
			}
		}
		highestTime_ = packet.getHighestTime();
	}

	/**
	 * Generates the accumulation frame (potential surface) at the time of the
	 * last consumed event.
	 * The function returns an OpenCV frame to work
	 * with. The frame has data type CV_32F.
	 * @param outFrame the frame to copy the data to
	 */
	void generateFrame(cv::Mat &outFrame) {
		if (synchronousDecay_ && decayFunction_ != NONE && decayFunction_ != STEP) {
			for (int r = 0; r < potentialSurface_.rows; r++) {
				for (int c = 0; c < potentialSurface_.cols; c++) {
					decay(static_cast<int16_t>(c), static_cast<int16_t>(r), highestTime_);
				}
			}
		}
		potentialSurface_.copyTo(outFrame);
		// in case of step decay function, clear potential surface
		if (decayFunction_ == STEP) {
			potentialSurface_.setTo(neutralPotential_);
		}
	}

	/**
	 * Generates the accumulation frame (potential surface) at the time of the
	 * last consumed event.
	 * The function returns an OpenCV frame to work
	 * with. The frame has data type CV_32F.
	 * @return An OpenCV frame with data type CV_32F containing the potential surface.
	 */
	cv::Mat generateFrame() {
		cv::Mat out;
		generateFrame(out);
		return out;
	}

	/**
	 * Clears the potential surface by setting it to the neutral value.
	 * This function does not reset the time surface.
	 */
	void clear() {
		potentialSurface_ = cv::Mat(shape_, CV_32F, static_cast<double>(neutralPotential_));
	}

	// setters
	/**
	 * If set to true, all events will incur a positive contribution to the
	 * potential surface
	 * @param rectifyPolarity The new value to set
	 */
	void setRectifyPolarity(bool rectifyPolarity) {
		Accumulator::rectifyPolarity_ = rectifyPolarity;
	}

	/**
	 * Contribution to the potential surface an event shall incur.
	 * This contribution is either counted positively (for positive events
	 * or when `rectifyPolatity` is set).
	 * @param eventContribution The contribution a single event shall incur
	 */
	void setEventContribution(float eventContribution) {
		Accumulator::eventContribution_ = eventContribution;
	}

	/**
	 * @param maxPotential the max potential at which the surface should be capped at
	 */
	void setMaxPotential(float maxPotential) {
		Accumulator::maxPotential_ = maxPotential;
	}

	/**
	 * @param neutralPotential The neutral potential to which the decay function should go.
	 * Exponential decay always goes to 0. The parameter is ignored there.
	 */
	void setNeutralPotential(float neutralPotential) {
		Accumulator::neutralPotential_ = neutralPotential;
	}

	/**
	 * @param minPotential the min potential at which the surface should be capped at
	 */
	void setMinPotential(float minPotential) {
		Accumulator::minPotential_ = minPotential;
	}

	/**
	 * @param decayFunction The decay function the module should use to perform the decay
	 */
	void setDecayFunction(Decay decayFunction) {
		Accumulator::decayFunction_ = decayFunction;
	}

	/**
	 * The decay param. This is slope for linear decay, tau for exponential decay
	 * @param decayParam The param to be used
	 */
	void setDecayParam(double decayParam) {
		Accumulator::decayParam_ = decayParam;
	}

	/**
	 * If set to true, all valued get decayed to the frame generation time at
	 * frame generation. If set to false, the values only get decayed on activity.
	 * @param synchronousDecay the new value for synchronoues decay
	 */
	void setSynchronousDecay(bool synchronousDecay) {
		Accumulator::synchronousDecay_ = synchronousDecay;
	}

	[[nodiscard]] bool isRectifyPolarity() const {
		return rectifyPolarity_;
	}

	[[nodiscard]] float getEventContribution() const {
		return eventContribution_;
	}

	[[nodiscard]] float getMaxPotential() const {
		return maxPotential_;
	}

	[[nodiscard]] float getNeutralPotential() const {
		return neutralPotential_;
	}

	[[nodiscard]] float getMinPotential() const {
		return minPotential_;
	}

	[[nodiscard]] Decay getDecayFunction() const {
		return decayFunction_;
	}

	[[nodiscard]] double getDecayParam() const {
		return decayParam_;
	}

	[[nodiscard]] const cv::Size &getShape() const {
		return shape_;
	}
};

/**
 * TimeSurface class that builds the surface of the occurrences of the last
 * timestamps.
 */
class TimeSurface {
public:
	/**
	 * Creates a new time surface accumulator with the given size
	 * @param size The size (in pixels) for the time surface integrator
	 */
	explicit TimeSurface(int16_t rows, int16_t cols) : mTimeSurface(TimeMat(rows, cols)) {
	}

	TimeSurface(const cv::Size &size) :
		TimeSurface(static_cast<int16_t>(size.height), static_cast<int16_t>(size.width)) {
	}

	/**
	 * Updates the current time surface with the events in the given store
	 * @param store The packet of events to apply to the store
	 */
	void accept(const EventStore &store) {
		for (const Event &event : store) {
			mTimeSurface.at(event.y(), event.x()) = event.timestamp();
		}
	}

	/**
	 * Returns the current time surface
	 * @return An OpenCV Matrix containing the current time surface.
	 */
	[[nodiscard]] const TimeMat &getTimeSurface() const {
		return mTimeSurface;
	}

	/**
	 * Returns the last event time at the given time
	 * @param x the x coordinate to obtain the last event time
	 * @param y the y coordinate to obtain the last event time
	 * @return The time at which the last event happened at this location.
	 * -1 if no event hasn't happened there yet.
	 */
	[[nodiscard]] const int64_t &at(int16_t x, int16_t y) const noexcept {
		return mTimeSurface.at(y, x);
	}

	/**
	 * Returns the last event time at the given time
	 * @param x the x coordinate to obtain the last event time
	 * @param y the y coordinate to obtain the last event time
	 * @return The time at which the last event happened at this location.
	 * -1 if no event hasn't happened there yet.
	 */
	[[nodiscard]] int64_t &at(int16_t x, int16_t y) noexcept {
		return mTimeSurface.at(y, x);
	}

private:
	TimeMat mTimeSurface;
};

} // namespace dv

#endif // DV_PROCESSING_FRAME_HPP
