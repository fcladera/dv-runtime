#ifndef STEREO_RECTIFICATION_FRAMES_HPP
#define STEREO_RECTIFICATION_FRAMES_HPP

#include "dv-sdk/data/frame.hpp"

#include "stereo_rectification.hpp"

class StereoRectificationFrames : public StereoRectification {
public:
	static const char *initDescription();
	static void initInputs(dv::InputDefinitionList &in);
	static void initOutputs(dv::OutputDefinitionList &out);

	StereoRectificationFrames();

	void run() override;

private:
	cv::Mat remap1[NUM_CAMERAS];
	cv::Mat remap2[NUM_CAMERAS];

	void initStereoRectification(
		const CameraInfo loaded[NUM_CAMERAS], const cv::Mat R, const cv::Mat T, const double alpha) override;
	void stereoRectifyFrame(
		const dv::InputDataWrapper<dv::Frame> &in, dv::OutputDataWrapper<dv::Frame> &out, size_t cam);
};

#endif // STEREO_RECTIFICATION_FRAMES_HPP
