#define FLATBUFFERS_DEBUG_VERIFICATION_FAILURE 1
#define FLATBUFFERS_TRACK_VERIFIER_BUFFER_SIZE 1

#include "dv-sdk/utils.h"

#include "../../modules/input/dv_input.hpp"
#include "../../src/main.hpp"

#include <boost/filesystem.hpp>
#include <boost/nowide/args.hpp>
#include <boost/nowide/filesystem.hpp>
#include <boost/nowide/iostream.hpp>
#include <boost/program_options.hpp>
#include <fmt/format.h>
#include <iostream>
#include <map>

namespace po = boost::program_options;

[[noreturn]] static inline void printHelpAndExit(const po::options_description &desc) {
	boost::nowide::cout << std::endl << desc << std::endl;
	exit(EXIT_FAILURE);
}

static constexpr auto evtType
	= dv::Types::makeTypeDefinition<dv::EventPacket, dv::Event>("Array of events (polarity ON/OFF).");
static constexpr auto frmType = dv::Types::makeTypeDefinition<dv::Frame, dv::Frame>("Standard frame (8-bit image).");
static constexpr auto imuType
	= dv::Types::makeTypeDefinition<dv::IMUPacket, dv::IMU>("Inertial Measurement Unit data samples.");
static constexpr auto trigType
	= dv::Types::makeTypeDefinition<dv::TriggerPacket, dv::Trigger>("External triggers and special signals.");
static constexpr auto bboxType
	= dv::Types::makeTypeDefinition<dv::BoundingBoxPacket, dv::BoundingBox>("Bounding boxes for object tracking.");
static constexpr auto nullType
	= dv::Types::Type("NULL", "Placeholder for errors.", 0, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr);

static inline dv::Types::Type getDefaultTypes(const uint32_t typeId) {
	switch (typeId) {
		case evtType.id:
			return evtType;

		case frmType.id:
			return frmType;

		case imuType.id:
			return imuType;

		case trigType.id:
			return trigType;

		case bboxType.id:
			return bboxType;

		default:
			return nullType;
	}
}

static void setupLibraryHooks() {
	// Setup internal function pointers for public support library.
	static dv::SDKLibFunctionPointers libFuncPtrs;

	libFuncPtrs.log = []([[maybe_unused]] dv::logLevel level, std::string_view msg) {
		boost::nowide::cout << ">>> " << msg << std::endl << std::endl;
	};

	libFuncPtrs.getTypeInfoCharString = [](const char *typeStr, [[maybe_unused]] const dv::Module *m) {
		return getDefaultTypes(dvTypeIdentifierToId(typeStr));
	};

	libFuncPtrs.getTypeInfoIntegerID = [](const uint32_t typeInt, [[maybe_unused]] const dv::Module *m) {
		return getDefaultTypes(typeInt);
	};

	dv::SDKLibInit(&libFuncPtrs);
}

int main(int argc, char **argv) {
	// Setup internal function pointers for public support library.
	setupLibraryHooks();

	// UTF-8 support for file paths and CLI arguments on Windows.
	boost::nowide::nowide_filesystem();
	boost::nowide::args utfArgs(argc, argv);

	po::options_description cliDescription("Command-line options");
	cliDescription.add_options()("help,h", "print help text")("file,f", po::value<std::string>(), "File to examine")(
		"verbose,v", "Print full packet table");

	po::positional_options_description p;
	p.add("file", -1);

	po::variables_map cliVarMap;
	try {
		po::store(po::command_line_parser(argc, argv).options(cliDescription).positional(p).run(), cliVarMap);
		po::notify(cliVarMap);
	}
	catch (...) {
		boost::nowide::cout << "Failed to parse command-line options!" << std::endl;
		printHelpAndExit(cliDescription);
	}

	// Parse/check command-line options.
	if (cliVarMap.count("help")) {
		printHelpAndExit(cliDescription);
	}

	std::string file;
	if (cliVarMap.count("file")) {
		file = cliVarMap["file"].as<std::string>();
	}
	else {
		printHelpAndExit(cliDescription);
	}

	bool verbose = false;
	if (cliVarMap.count("verbose")) {
		verbose = true;
	}

	boost::filesystem::path filePath{file};

	boost::nowide::ifstream fileStream;
	dv::InputInformation fileInfo;

	try {
		filePath = boost::filesystem::absolute(filePath).make_preferred();

		fileStream = dv::InputDecoder::openFile(filePath);

		fileInfo = dv::InputDecoder::parseHeader(fileStream, nullptr, false);
	}
	catch (const std::exception &ex) {
		boost::nowide::cerr << fmt::format(FMT_STRING("File '{:s}': {:s}"), file, ex.what()) << std::endl;

		return (EXIT_FAILURE);
	}

	boost::nowide::cout << "File path (absolute): " << filePath << std::endl;
	boost::nowide::cout << "File path (canonical): " << boost::filesystem::canonical(filePath).make_preferred()
						<< std::endl;
	boost::nowide::cout << "File size (OS): " << boost::filesystem::file_size(filePath) << std::endl;
	boost::nowide::cout << "File size (Parser): " << fileInfo.fileSize << std::endl;
	boost::nowide::cout << "Compression: " << dv::EnumNameCompressionType(fileInfo.compression) << std::endl;
	boost::nowide::cout << "Timestamp lowest: " << fileInfo.timeLowest << std::endl;
	boost::nowide::cout << "Timestamp highest: " << fileInfo.timeHighest << std::endl;
	boost::nowide::cout << "Timestamp difference: " << fileInfo.timeDifference << std::endl;
	boost::nowide::cout << "Timestamp shift: " << fileInfo.timeShift << std::endl;

	if (fileInfo.streams.empty()) {
		boost::nowide::cerr << "Streams: NONE FOUND" << std::endl;
	}
	else {
		for (const auto &st : fileInfo.streams) {
			boost::nowide::cout << fmt::format(
				FMT_STRING("Stream {:d}: {:s} - {:s}"), st.id, st.name, st.typeIdentifier)
								<< std::endl;
		}
	}

	boost::nowide::cout << "DataTable file position: " << fileInfo.dataTablePosition << std::endl;
	boost::nowide::cout << "DataTable file size: " << fileInfo.dataTableSize << std::endl;
	boost::nowide::cout << "DataTable elements: " << fileInfo.dataTable->Table.size() << std::endl;

	if (fileInfo.dataTable->Table.empty()) {
		boost::nowide::cerr << "DataTable: NONE FOUND" << std::endl;
	}
	else {
		if (verbose) {
			std::map<int32_t, int64_t> lastTimestamp;

			for (const auto &pkt : fileInfo.dataTable->Table) {
				boost::nowide::cout << fmt::format(
					FMT_STRING("Packet at {:d}: StreamID {:d} - Size {:d} - NumElements {:d} - "
							   "TimestampStart {:d} - TimestampEnd {:d}"),
					pkt.ByteOffset, pkt.PacketInfo.StreamID(), pkt.PacketInfo.Size(), pkt.NumElements,
					pkt.TimestampStart, pkt.TimestampEnd)
									<< std::endl;

				if (pkt.TimestampEnd < pkt.TimestampStart) {
					boost::nowide::cout << fmt::format(
						FMT_STRING("Packet at {:d}: ERROR: timestamps out of order inside packet, difference {:d}"),
						pkt.ByteOffset, (pkt.TimestampEnd - pkt.TimestampStart))
										<< std::endl;
				}

				if (pkt.TimestampStart < lastTimestamp[pkt.PacketInfo.StreamID()]) {
					boost::nowide::cout << fmt::format(
						FMT_STRING("Packet at {:d}: ERROR: timestamps out of order between packets, difference {:d}"),
						pkt.ByteOffset, (pkt.TimestampStart - lastTimestamp[pkt.PacketInfo.StreamID()]))
										<< std::endl;
				}

				lastTimestamp[pkt.PacketInfo.StreamID()] = pkt.TimestampEnd;
			}
		}
	}

	return (EXIT_SUCCESS);
}
